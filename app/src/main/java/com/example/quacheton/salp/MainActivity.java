package com.example.quacheton.salp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.vuforia.Vuforia;

public class MainActivity extends AppCompatActivity {

    // Initialize with invalid value:
    private int mProgressValue = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Vuforia.setInitParameters(this, Vuforia.GL_20, getString(R.string.licence_key_vuforia));
        do {
            mProgressValue = Vuforia.init();
        }
        while (mProgressValue >= 0 && mProgressValue < 100);
    }
}
